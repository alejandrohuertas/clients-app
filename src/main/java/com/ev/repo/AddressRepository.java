package com.ev.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ev.domain.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

}
