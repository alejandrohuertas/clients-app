package com.ev.repo;

import com.ev.domain.PhoneNumber;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneRepository extends CrudRepository<PhoneNumber, Integer> {

  void save(List<PhoneRepository> phones);
}
