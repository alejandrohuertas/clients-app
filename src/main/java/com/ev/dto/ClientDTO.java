package com.ev.dto;

import java.util.Collections;
import java.util.List;

public class ClientDTO {

  private Integer id;
  private String firstName;
  private String lastName;
  private AddressDTO address;
  private List<PhoneDTO> phones;

  private ClientDTO(Integer id, String firstName, String lastName, AddressDTO address, List<PhoneDTO> phones) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.address = address;
    this.phones = phones;
  }

  public ClientDTO() {
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public AddressDTO getAddress() {
    return address;
  }

  public int getId() {
    return id;
  }

  public List<PhoneDTO> getPhones() {
    return phones!=null? phones : Collections.emptyList();
  }

  public static class ClientDTOBuilder {

    private Integer id;
    private String firstName;
    private String lastName;
    private AddressDTO address;
    private List<PhoneDTO> phones;

    public ClientDTOBuilder withLastName(String lastName){
      this.lastName = lastName;
      return this;
    }

    public ClientDTOBuilder withFirstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public ClientDTOBuilder withAddress(AddressDTO address) {
      this.address = address;
      return this;
    }

    public ClientDTOBuilder withId(Integer id) {
      this.id = id;
      return this;
    }

    public ClientDTOBuilder withPhones(List<PhoneDTO> phones) {
      this.phones = phones;
      return this;
    }

    public ClientDTO build(){
      return new ClientDTO(id, firstName, lastName, address, phones);
    }
  }

}
