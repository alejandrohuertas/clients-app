package com.ev.dto;

public class AddressDTO {
  private String city;
  private String street;
  private String zipCode;

  private AddressDTO(String city, String street, String zipCode) {
    this.city = city;
    this.street = street;
    this.zipCode = zipCode;
  }

  public AddressDTO() {
  }

  public String getCity() {
    return city;
  }

  public String getStreet() {
    return street;
  }

  public String getZipCode() {
    return zipCode;
  }

  public static class AddressDTOBuilder {
    private String city;
    private String street;
    private String zipCode;

    public AddressDTOBuilder withCity(String city) {
      this.city = city;
      return this;
    }

    public AddressDTOBuilder withStreet(String street) {
      this.street = street;
      return this;
    }

    public AddressDTOBuilder withZipCode(String zipCode) {
      this.zipCode = zipCode;
      return this;
    }

    public AddressDTO build() {
      return new AddressDTO(city, street, zipCode);
    }
  }
}
