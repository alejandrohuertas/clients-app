package com.ev.dto;

public class PhoneDTO {
  private String type;
  private String number;


  public PhoneDTO() {
  }

  private PhoneDTO(String type, String number) {
    this.type = type;
    this.number = number;
  }

  public String getType() {
    return type;
  }

  public String getNumber() {
    return number;
  }

  public static class PhoneDTOBuilder {
    private String type;
    private String number;

    public PhoneDTOBuilder withType(String type) {
      this.type = type;
      return this;
    }

    public PhoneDTOBuilder withNumber(String number) {
      this.number = number;
      return this;
    }


    public PhoneDTO build() {
      return new PhoneDTO(type, number);
    }
  }
}
