package com.ev.domain;

import org.hibernate.engine.internal.Cascade;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

@Entity
@Table(name = "client", uniqueConstraints = @UniqueConstraint(name = "uc_name", columnNames = { "firstName","lastName" }))
public class Client {

  @Id
  @GeneratedValue
  private Integer id;

  @Column(nullable = false)
  private String firstName;

  @Column(nullable = false)
  private String lastName;

  @OneToOne (cascade = CascadeType.ALL)
  private Address address;

  @OneToMany(cascade = CascadeType.ALL)
  private List<PhoneNumber> phones;

  public Client() {
  }

  public Client(String firstName, String lastName, List<PhoneNumber> phones, Address address) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phones = phones;
    this.address = address;
  }

  public String getLastName() {
    return lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public Address getAddress() {
    return address;
  }

  public List<PhoneNumber> getPhones() {
    return phones;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }
}
