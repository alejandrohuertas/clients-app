package com.ev.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address {

  @Id
  @GeneratedValue
  private Integer id;
  private String city;
  private String street;
  private String zipCode;

  public Address(String city, String street, String zipCode) {
    this.city = city;
    this.street = street;
    this.zipCode = zipCode;
  }

  public Address() {

  }

  public String getCity() {
    return city;
  }

  public String getStreet() {
    return street;
  }

  public String getZipCode() {
    return zipCode;
  }
}
