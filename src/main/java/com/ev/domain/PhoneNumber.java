package com.ev.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "phone_number", uniqueConstraints = @UniqueConstraint(name = "uc_phone", columnNames = { "type", "number" }))
public class PhoneNumber {

  @Id
  @GeneratedValue
  private Integer id;
  //TODO: can be changed to an ENUM
  private String type;
  private String number;


  public PhoneNumber(String type, String number) {
    this.type = type;
    this.number = number;
  }

  public PhoneNumber() {
  }

  public String getType() {
    return type;
  }

  public String getNumber() {
    return number;
  }
}
