package com.ev.controller;

import com.ev.controller.mapper.ClientMapper;
import com.ev.domain.Client;
import com.ev.dto.ClientDTO;
import com.ev.exception.ConstraintsViolationException;
import com.ev.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/clients")
public class ClientController {

  private ClientService clientService;

  @Autowired
  public ClientController(ClientService clientService) {
    this.clientService = clientService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity createClient(@Valid @RequestBody ClientDTO clientDTO) throws ConstraintsViolationException {
    Client client = ClientMapper.makeClientFromDTO(clientDTO);
    return new ResponseEntity<>(ClientMapper.makeClientDTO(clientService.createClient(client)), HttpStatus.CREATED);
  }

  @DeleteMapping("/{clientId}")
  public ResponseEntity deleteClient(@PathVariable Integer clientId) {
    try{
      clientService.deleteClient(clientId);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }catch (EntityNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/{clientId}")
  public ResponseEntity getClientById(@Valid @PathVariable Integer clientId) {
    try {
      Client client = clientService.findClient(clientId);
      return new ResponseEntity<>(ClientMapper.makeClientDTO(client), HttpStatus.OK);
    } catch (EntityNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping
  public List<ClientDTO> getAllClients() {
    return ClientMapper.makeClientsDTOList(clientService.getClients());
  }

}
