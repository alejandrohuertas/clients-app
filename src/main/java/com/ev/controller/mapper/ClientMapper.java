package com.ev.controller.mapper;

import com.ev.domain.Client;
import com.ev.dto.AddressDTO;
import com.ev.dto.ClientDTO;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClientMapper {
  public static Client makeClientFromDTO(ClientDTO clientDTO) {
    Optional<AddressDTO> address = Optional.ofNullable(clientDTO.getAddress());
    return new Client(clientDTO.getFirstName(), clientDTO.getLastName(), PhoneMapper.makePhonesList(clientDTO.getPhones()),
        AddressMapper.makeAddressFromDTO(address.orElse(null)));
  }

  public static ClientDTO makeClientDTO(Client client) {
    ClientDTO.ClientDTOBuilder builder = new ClientDTO.ClientDTOBuilder();

    return builder.withLastName(client.getLastName()).withFirstName(client.getFirstName()).withId(client.getId())
        .withPhones(PhoneMapper.makePhonesDTOList(client.getPhones()))
        .withAddress(AddressMapper.makeAddressDTO(client.getAddress()).orElse(null)).build();

  }

  public static List<ClientDTO> makeClientsDTOList(List<Client> clients) {
    return clients.stream().map(ClientMapper::makeClientDTO).collect(Collectors.toList());
  }
}
