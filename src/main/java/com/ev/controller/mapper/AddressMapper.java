package com.ev.controller.mapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ev.dto.AddressDTO;
import com.ev.domain.Address;

public class AddressMapper {
  public static Address makeAddressFromDTO(AddressDTO addressDTO) {
    if (addressDTO != null) {
      return new Address(addressDTO.getCity(), addressDTO.getStreet(), addressDTO.getZipCode());
    } else {
      return null;
    }
  }

  public static Optional<AddressDTO> makeAddressDTO(Address address) {
    if (address!=null) {
      return Optional
          .ofNullable(new AddressDTO.AddressDTOBuilder().withCity(address.getCity()).withStreet(address.getStreet()).withZipCode(address.getZipCode()).build());
    } else{
      return Optional.empty();
    }
  }

  public static List<AddressDTO> makeClientsDTOList(List<Address> address) {
    return address.stream().map(a -> makeAddressDTO(a).orElse(null)).collect(Collectors.toList());
  }
}
