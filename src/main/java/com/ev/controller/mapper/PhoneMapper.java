package com.ev.controller.mapper;

import com.ev.domain.PhoneNumber;
import com.ev.dto.PhoneDTO;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PhoneMapper {
  public static PhoneNumber makePhoneFromDTO(PhoneDTO phoneDTO) {
    if (phoneDTO != null) {
      return new PhoneNumber(phoneDTO.getType(), phoneDTO.getNumber());
    } else {
      return null;
    }
  }

  public static Optional<PhoneDTO> makePhoneNumberDTO(PhoneNumber address) {
    if (address!=null) {
      return Optional
          .ofNullable(new PhoneDTO.PhoneDTOBuilder().withType(address.getType()).withNumber(address.getNumber()).build());
    } else{
      return Optional.empty();
    }
  }

  public static List<PhoneDTO> makePhonesDTOList(List<PhoneNumber> phoneNumberList) {
    return phoneNumberList.stream().map(a -> makePhoneNumberDTO(a).orElse(null)).collect(Collectors.toList());
  }

  public static List<PhoneNumber> makePhonesList(List<PhoneDTO> phonesDTOList) {
    return phonesDTOList.stream().map(PhoneMapper::makePhoneFromDTO).collect(Collectors.toList());
  }
}
