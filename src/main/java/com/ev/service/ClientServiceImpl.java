package com.ev.service;

import com.ev.domain.Address;
import com.ev.domain.Client;
import com.ev.domain.PhoneNumber;
import com.ev.exception.ConstraintsViolationException;
import com.ev.repo.AddressRepository;
import com.ev.repo.ClientRepository;
import com.ev.repo.PhoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.text.MessageFormat;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService{

  final ClientRepository clientRepository;
  final AddressRepository addressRepository;
  final PhoneRepository phoneRepository;
  private static final Logger LOG = LoggerFactory.getLogger(ClientServiceImpl.class);

  public ClientServiceImpl(ClientRepository clientRepository, AddressRepository addressRepository, PhoneRepository phoneRepository) {
    this.clientRepository = clientRepository;
    this.addressRepository = addressRepository;
    this.phoneRepository = phoneRepository;
  }

  @Override
  public Client createClient(Client client) throws ConstraintsViolationException {
    Client newClient;
    try
    {
      newClient = clientRepository.save(client);
    }
    catch (DataIntegrityViolationException e) {
      LOG.error("Some constraints violations are thrown due to client creation", e);
      throw new ConstraintsViolationException("Some constraints violations are thrown due to client creation");
    }
    return newClient;
  }

  @Override
  public void deleteClient(Integer clientId) throws EntityNotFoundException {
    Client client = findClient(clientId);
    clientRepository.delete(client);
  }

  @Override
  public Client findClient(Integer clientId) {
    Client client = clientRepository.findOne(clientId);
    if (client == null) {
      String errorMessage = MessageFormat.format("Client with id {0} was not found", clientId);
      LOG.error(errorMessage);
      throw new EntityNotFoundException("Could not find client with id: " + client);
    }
    return client;
  }

  @Override
  public List<Client> getClients() {
    return clientRepository.findAll();
  }
}
