package com.ev.service;

import com.ev.domain.Client;
import com.ev.exception.ConstraintsViolationException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface ClientService {

  Client createClient (Client client) throws ConstraintsViolationException;

  void deleteClient (Integer clientId) throws EntityNotFoundException;

  Client findClient(Integer clientId);

  List<Client> getClients ();

}
