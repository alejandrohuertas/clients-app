package com.ev.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import com.ev.exception.ConstraintsViolationException;
import com.ev.repo.PhoneRepository;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ev.domain.Address;
import com.ev.domain.Client;
import com.ev.repo.AddressRepository;
import com.ev.repo.ClientRepository;

public class ClientServiceTest {

  @Mock
  private ClientRepository clientRepository;

  @Mock
  private AddressRepository addressRepository;

  @Mock
  private PhoneRepository phoneRepository;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void givenValidRequestWhenSearchingForAllClientsThenReturnAllClients() {

    // Arrange
    ClientService clientService = new ClientServiceImpl(clientRepository, addressRepository, phoneRepository);
    Client client1 = aClient("test name", "last");
    Client client2 = aClient("test name", "Johnson");
    Client client3 = aClient("test name", "Harrison");
    List<Client> clientsExpected = Lists.newArrayList(client1, client2, client3);

    when(clientRepository.findAll()).thenReturn(clientsExpected);

    // Act
    List<Client> clientsReturned = clientService.getClients();

    // Assert
    assertEquals(clientsExpected.size(), clientsReturned.size());
    Assert.assertArrayEquals(clientsExpected.toArray(), clientsReturned.toArray());
  }

  private Client aClient(String name, String lastName) {
    return new Client(name, lastName, Collections.emptyList(), new Address());
  }

  private Client aClientWithId(int id, String name, String last) {
    Client client = aClient(name, last);
    client.setId(id);
    return client;
  }


  @Test
  public void givenAValidClientWhenCreatingThenSaveIt() throws ConstraintsViolationException {

    //Arrange
    ClientService clientService = new ClientServiceImpl(clientRepository, addressRepository, phoneRepository);

    Client client = aClient("John", "Doe");
    //Act
    clientService.createClient(client);
    //Assert
    verify(clientRepository).save(client);
  }


  @Test
  public void givenValidIdWhenSearchingForAClientThenReturnTheMatchingOne() {

    //Arrange
    ClientService clientService = new ClientServiceImpl(clientRepository, addressRepository, phoneRepository);
    Client clientExpected = aClientWithId(1,"test name", "LAstName");
    Integer id = clientExpected.getId();
    when(clientRepository.findOne(id)).thenReturn(clientExpected);

    //Act
    Client clientReturned = clientService.findClient(id);

    //Assert
    assertEquals(clientExpected, clientReturned);
  }

  @Test
  public void givenValidIdWhenDeletingAClientThenDeleteTheMatchingOne() {

    //Arrange
    ClientService clientService = new ClientServiceImpl(clientRepository, addressRepository, phoneRepository);
    Client client = aClientWithId(1,"test name", "LAstName");
    Integer id = client.getId();
    when(clientRepository.findOne(id)).thenReturn(client);

    //Act
    clientService.deleteClient(id);

    //Assert
    verify(clientRepository).delete(client);
  }
}