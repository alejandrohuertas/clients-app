package com.ev;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ev.controller.ClientController;
/**
 * Created by AHuertasA on 17/05/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientsAppTest {

  @Autowired
  private ClientController controller;

  @Test
  public void contextLoads() {
    assertThat(controller).isNotNull();
  }

}