package com.ev.controller;

import com.ev.controller.mapper.ClientMapper;
import com.ev.domain.Address;
import com.ev.domain.Client;
import com.ev.dto.AddressDTO;
import com.ev.dto.ClientDTO;
import com.ev.dto.PhoneDTO;
import com.ev.exception.ConstraintsViolationException;
import com.ev.service.ClientService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by AHuertasA on 17/05/2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ClientControllerTest {

  private MockMvc mockMvc;

  @Mock
  private ClientService clientService;

  @InjectMocks
  private ClientController mainController = new ClientController(clientService);
  private Gson gson;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
    gson = new GsonBuilder().serializeNulls().create();
  }

  @Test
  public void givenAValidClientWhenCreatingThenCreateItSuccessfully() throws Exception {
    AddressDTO addressDTO = new AddressDTO.AddressDTOBuilder().withCity("Bogota").withStreet("Street 100").withZipCode("110000").build();
    PhoneDTO phone1 = aPhoneDTO("23324342");
    PhoneDTO phone2 = aPhoneDTO("233243311");
    PhoneDTO phone3 = aPhoneDTO("23004342");
    ClientDTO clientDTO = new ClientDTO.ClientDTOBuilder().withFirstName("James").withLastName("Bond").withAddress(addressDTO)
        .withPhones(Lists.newArrayList(phone1, phone2, phone3)).build();
    Client newClient = aClientWithIdFromDTO(1, clientDTO);
    when(clientService.createClient(any(Client.class))).thenReturn(newClient);

    String json = new Gson().toJson(clientDTO);
    mockMvc.perform(post("/api/clients").accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(json))
        .andExpect(status().isCreated());
  }

  @Test
  public void givenAInvalidConstraintsClientWhenCreatingThenReturnError() throws Exception {
    ClientDTO clientDTO = new ClientDTO.ClientDTOBuilder().withFirstName("James").withLastName("Rodriguez").withId(1).build();
    when(clientService.createClient(any(Client.class))).thenThrow(new ConstraintsViolationException("Client has fields that violates table constraints"));

    String json = new Gson().toJson(clientDTO);
    mockMvc.perform(post("/api/clients").accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(json))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenWhenGettingAllClientsThenReturnAllClients() throws Exception {
    Integer id = 4;
    Client client1 = aClientWithId(id);
    Client client2 = aClientWithId(id);
    Client client3 = aClientWithId(id);
    Client client4 = aClientWithId(id);

    List<Client> clientList = Lists.newArrayList(client1, client2, client3, client4);

    List<ClientDTO> clientDTOList = ClientMapper.makeClientsDTOList(clientList);

    String json = gson.toJson(clientDTOList);
    when(clientService.getClients()).thenReturn(clientList);
    mockMvc.perform(get("/api/clients").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(
        content().string(containsString(json)));
  }

  @Test
  public void givenAValidIdWhenDeletingAClientThenDeleteIt() throws Exception {
    Integer id = 4;
    Client client = aClientWithId(id);
    when(clientService.findClient(id)).thenReturn(client);
    mockMvc.perform(delete("/api/clients/" + id).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
  }

  @Test
  public void givenAInvalidIdWhenDeletingAClientThenReturnErrorResponse() throws Exception {
    Integer id = 4;

    when(clientService.findClient(id)).thenThrow(new EntityNotFoundException("Client with id " + id + " doesn't Exist"));
    mockMvc.perform(get("/api/clients/" + id).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
  }

  @Test
  public void givenAValidIdWhenGettingAClientThenReturnClient() throws Exception {
    Integer id = 4;
    Client client = aClientWithId(id);
    ClientDTO clientDTO = ClientMapper.makeClientDTO(client);

    String json = gson.toJson(clientDTO);
    when(clientService.findClient(id)).thenReturn(client);
    mockMvc.perform(get("/api/clients/" + id).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(
        content().string(containsString(json)));
  }

  @Test
  public void givenAInvalidIdWhenGettingAClientThenReturnErrorResponse() throws Exception {
    Integer id = 4;

    when(clientService.findClient(id)).thenThrow(new EntityNotFoundException("Client with id " + id + " doesnt Exist"));
    mockMvc.perform(get("/api/clients/" + id).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
  }

  private Client aClientWithId(Integer id) {
    Client client = new Client("Test Name for Client " + id, "last name", Collections.emptyList(), new Address());
    client.setId(id);
    return client;
  }

  private Client aClientWithIdFromDTO(int id, ClientDTO clientDTO) {
    Client client = ClientMapper.makeClientFromDTO(clientDTO);
    client.setId(id);
    return client;
  }

  private PhoneDTO aPhoneDTO(String number) {
    return new PhoneDTO.PhoneDTOBuilder().withNumber(number).withType("mobile").build();
  }
}